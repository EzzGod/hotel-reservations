from django.shortcuts import render, redirect
from .models import Accommodation, Car, AccommodationReview, CarReview, AccomodationCheckout,\
                     AccommodationOrder, OrderCar, HotelRoom, Hotel, RoomOrder, RoomCheckout, RoomPicture
from adminPanel.models import FooterDescription, City
from .forms import AccommodationReviewForm, CarReviewForm, AccommodationOrderForm, CarOrderForm, RoomOrderForm
from django.forms.formsets import formset_factory


def middle_accommodation(request):
    cities = City.objects.all()   
    context = {

        'cities': cities,
    }

    return render(request, 'personalTour/personalmiddle.html', context)


def archive_accommodation(request):

    accommodations = Accommodation.objects.all()

    context = {
        'accommodations': accommodations,
    }

    return render(request, 'personalTour/archive_accommodation.html', context)


def single_accommodation(request, id):
    accommodation = Accommodation.objects.get(id=id)
    hotel = Hotel.objects.get(hotel_name=accommodation)
    rooms = HotelRoom.objects.filter(hotel=hotel)
    reviews = AccommodationReview.objects.filter(accommodation_id=id)


    context = {

        'accommodation': accommodation,
        'reviews': reviews,
        'rooms':rooms,
    }

    return render(request, 'personalTour/accommodation_single.html', context)


def accommodation_review(request, id):
    if request.user.is_authenticated:
        tour = Accommodation.objects.get(id=id)
        if request.method == 'POST':
            form = AccommodationReviewForm(request.POST or None)
            if form.is_valid():
                data = form.save(commit=False)
                data.user = request.user
                data.accommodation = tour
                data.save()

                return redirect('personalTour:singleaccommodation', id)
        else:
            form = AccommodationReviewForm()

        return render(request, 'personalTour/accommodation_single.html', {'form': form})
    else:
        return redirect('accounts/login')






def archive_car(request):
    cars = Car.objects.all()


    context = {

        'cars': cars,

    }

    return render(request, 'personalTour/archive_car.html', context)


def single_car(request, id):
    car = Car.objects.get(id=id)
    reviews = CarReview.objects.filter(car_id=id)
 
    context = {

        'car': car,
        'reviews': reviews,
    }

    return render(request, 'personalTour/single_car.html', context)


def car_review(request, id):
    if request.user.is_authenticated:
        tour = Car.objects.get(id=id)
        if request.method == 'POST':
            form = CarReviewForm(request.POST or None)
            if form.is_valid():
                data = form.save(commit=False)
                data.user = request.user
                data.car = tour
                data.save()

                return redirect('personalTour:singlecar', id)
        else:
            form = CarReviewForm()

        return render(request, 'personalTour/single_car.html', {'form': form})
    else:
        return redirect('accounts/login')


def personaltour_checkout(request):
    try:
        gotten_ids = request.GET.get('roomsId').split()

    except:
        gotten_ids = []
    for id in gotten_ids:
        if len(RoomCheckout.objects.filter(user=request.user,
                                                    room_checkout=HotelRoom.objects.get(id=int(id)))) == 0:
            RoomCheckout(user=request.user, room_checkout=HotelRoom.objects.get(id=int(id))).save()
    checkouts = RoomCheckout.objects.filter(user=request.user, )
    room_ids = ()
    for i in checkouts:
       room_ids += (i.room_checkout.id,)
    room_formset = formset_factory(RoomOrderForm, extra=len(checkouts))
    formset = room_formset()
    print(room_ids)
    room_dict = {}

    for i in range(len(room_ids)):
        room_dict[HotelRoom.objects.get(id=room_ids[i])]=(formset[i], RoomPicture.objects.filter(room__id=room_ids[i]))
    print(room_dict)
    order_dates = {}
    orders = RoomOrder.objects.filter(room__id__in=room_ids)
    for order in orders:
        if order.room.id in order_dates.keys():
            order_dates[order.room.id].append((str(order.room_order_start_date),
                                               str(order.room_order_end_date)))
        else:
            order_dates[order.room.id] = [(str(order.room_order_start_date),
                                           str(order.room_order_end_date))]

    successfully_dict = {}

    if request.method=="POST":
        print("shemovediiiiiiiiiiiiiiiiii")
        formset = room_formset(request.POST or None)
        if formset.is_valid():
            index = 0
            for room,form in room_dict.items():
                data = form[0].save(commit=False)
                data.room = room
                data.user = request.user
                print("order dates===========", order_dates)
                if data.room.id in order_dates.keys():
                    order_dates[data.room.id].append(((
                                            request.POST.get('form-' + str(index) + '-room_order_start_date')),
                                            (request.POST.get('form-' + str(index) + '-room_order_end_date'))))
                    order_dates[data.room.id].sort()

                    print(request.POST.get('form-' + str(index) + '-room_order_start_date'))
                    print("order dates===========", order_dates)
                    order_index = order_dates[data.room.id].index((
                        request.POST.get('form-' + str(index) + '-room_order_start_date'),
                        request.POST.get('form-' + str(index) + '-room_order_end_date')))

                    if order_index == 0:
                        if order_dates[data.room.id][order_index][1]<order_dates[data.room.id][order_index+1][0]:
                            data.room_order_start_date = request.POST.get('form-' + str(index) + '-room_order_start_date')
                            data.room_order_end_date = request.POST.get('form-' + str(index) + '-room_order_end_date')
                            data.save()
                            successfully_dict[data.id]=True
                    
                    elif order_index == len(order_dates[data.room.id])-1:
                        print("elfishi movedi")
                        if order_dates[data.room.id][order_index][0]>order_dates[data.room.id][order_index-1][1]:
                            data.room_order_start_date = request.POST.get('form-' + str(index) + '-room_order_start_date')
                            data.room_order_end_date = request.POST.get('form-' + str(index) + '-room_order_end_date')
                            data.save()
                            successfully_dict[data.id]=True
                        print("elifidan wavedi")
                    else:
                        if order_dates[data.room.id][order_index][0]>order_dates[data.room.id][order_index-1][1] \
                                and order_dates[data.room.id][order_index][1]<order_dates[data.room.id][order_index+1][0]:
                            data.room_order_start_date = request.POST.get('form-' + str(index) + '-room_order_start_date')
                            data.room_order_end_date = request.POST.get('form-' + str(index) + '-room_order_end_date')
                            data.save()
                            successfully_dict[data.id]=True

                else:
                    print("elsshi movedi")
                    order_dates[data.room.id] = [((request.POST.get('form-' + str(index) + '-room_order_start_date')),
                                                (request.POST.get('form-' + str(index) + '-room_order_end_date')))]
                    order_dates[data.room.id].sort()

                    if len(order_dates)==1:
                        data.room_order_start_date = request.POST.get('form-' + str(index) + '-room_order_start_date')
                        data.room_order_end_date = request.POST.get('form-' + str(index) + '-room_order_end_date')
                        data.save()
                        successfully_dict[data.id]=True

                index +=1
    
            if len(successfully_dict)<len(room_dict):
                for i in successfully_dict.keys():
                    order = RoomOrder.objects.get(id=i).delete()
                return redirect('personalTour:personalCheckout')
            else:
                for id in room_dict.keys():
                    print(id)
                    checkout = RoomCheckout.objects.get(user=request.user, room_checkout=id).delete()
                return redirect('themedTour:thankyou')
    total_price = 0
    for price in room_dict.keys():
        total_price += price.price

    print(total_price)
    context = {
        'formset':formset,
        'room_dict':room_dict,
        'total_price':total_price,

    }
    return render(request, 'personalTour/order_pack_personal.html', context)


def delete_checkout(request, id):
    room = HotelRoom.objects.get(id=id)
    checkout = RoomCheckout.objects.get(room_checkout=room, user=request.user)
    checkout.delete()
    return redirect('personalTour:personalCheckout')

def car_order(request, id):
    car = Car.objects.get(id=id)
    car_orders = OrderCar.objects.filter(car=id)
    car_order_dates = []
    for order in car_orders:
        car_order_dates.append((str(order.car_order_start_date), str(order.car_order_end_date)))
    form = CarOrderForm()
    print(car_order_dates)
    if request.user.is_authenticated:
        if request.method == "POST":
            form = CarOrderForm(request.POST or None)
            if form.is_valid():
                data = form.save(commit=False)
                data.user = request.user
                data.car = car
                car_order_dates.append((str(data.car_order_start_date), str(data.car_order_end_date)))
                car_order_dates.sort()
                order_index = car_order_dates.index((str(data.car_order_start_date), str(data.car_order_end_date)))
                print(car_order_dates)
                if order_index == 0:
                    if car_order_dates[order_index][1] < car_order_dates[order_index+1][0]:
                        data.save()
                elif order_index == len(car_order_dates):
                    if  car_order_dates[order_index][0] > car_order_dates[order_index-1][1]:
                        data.save()   
                else:
                    if car_order_dates[order_index][0] > car_order_dates[order_index-1][1] and car_order_dates[order_index][1] < car_order_dates[order_index+1][0]:
                        data.save()

                return redirect('themedTour:thankyou')        

    context = {
        'car':car,
        'form':form,
    }

    return render(request, 'personalTour/checkout_car.html', context)
