from django.urls import path
from . import views

app_name = "personalTour"

urlpatterns = [
    path('accomodationmiddle', views.middle_accommodation, name='accomodationMiddle'),
    path("archiveaccommodation", views.archive_accommodation, name="archiveaccommodation"),
    path("singleaccommodation/<int:id>", views.single_accommodation, name="singleaccommodation"),
    path('accommodationreview/<int:id>', views.accommodation_review, name='accommodationreview'),
    path("archivecar", views.archive_car, name="archivecar"),
    path("singlecar/<int:id>", views.single_car, name="singlecar"),
    path('carreview/<int:id>', views.car_review, name='carreview'),
    path('personalcheckout/', views.personaltour_checkout, name='personalCheckout'),
    path('checkoutcar/<int:id>', views.car_order, name='checkoutCar'),
    path('checkoutdelete/<int:id>', views.delete_checkout, name='checkoutDelete'),
]
