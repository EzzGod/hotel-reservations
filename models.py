from django.db import models
from adminPanel.models import Country, CarMark
from django.core.validators import MaxValueValidator
from registration.models import Account


class Accommodation(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=256)
    description = models.TextField(blank=True, null=True)
    presentation_text = models.TextField(blank=True, null=True)
    destination = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True)
    address = models.CharField(max_length=256)
    price = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    picture1 = models.ImageField(upload_to='images/accomodation', default='images/brand-2.jpg')
    picture2 = models.ImageField(upload_to='images/accomodation', default='images/brand-2.jpg',
                                 null=True, blank=True)
    picture3 = models.ImageField(upload_to='images/accomodation', default='images/brand-2.jpg',
                                 null=True, blank=True)
    video = models.FileField(upload_to="accomodationvideos")
    ACCOMODATION_TYPES = (
        ("1", 'Hotel'),
        ("2", 'Apartment'),
    )
    accommodation_type = models.CharField(max_length=256, choices=ACCOMODATION_TYPES)
    permission = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Accommodation'


class Hotel(models.Model):
    hotel_name = models.ForeignKey(Accommodation, on_delete=models.CASCADE)
    identification_number = models.CharField(max_length=128)
    number_of_rooms = models.IntegerField(default=0)
    WiFi_connection = models.BooleanField(default=False)
    parking = models.BooleanField(default=False)
    bar = models.BooleanField(default=False)
    pool = models.BooleanField(default=False)


class HotelRoom(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    room_number = models.IntegerField()
    price = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    ROOM_TYPES = (
        ('1', 'Independent room'),
        ('2', 'Shared room'),
        ('3', 'Private room'),
    )
    room_type = models.CharField(max_length=256, choices=ROOM_TYPES)
    ROOM_DESCRIPTION = (
        ('1', 'Twin'),
        ('2', 'Double'),
        ('3', 'Triple'),
        ('4', 'Shared'),
    )
    room_description = models.CharField(max_length=256, choices=ROOM_DESCRIPTION)
    family = models.BooleanField(default=False)


class RoomOrder(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    room = models.ForeignKey(HotelRoom, on_delete=models.CASCADE)
    room_order_start_date = models.DateField()
    room_order_end_date = models.DateField()


class RoomCheckout(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    room_checkout = models.ForeignKey(HotelRoom, on_delete=models.CASCADE)


class Apartment(models.Model):
    apartment_name = models.ForeignKey(Accommodation, on_delete=models.CASCADE)
    CHOICE_OF_APARTMENTS = (
        ('1', 'Studio'),
        ('2', 'F1'),
        ('3', 'F2'),
        ('4', 'F3'),
        ('5', 'F4'),
        ('6', 'villa'),
    )
    choice_of_apartment = models.CharField(max_length=256, choices=CHOICE_OF_APARTMENTS)
    capacity = models.PositiveIntegerField(validators=[MaxValueValidator(20)])
    WiFi_connection = models.BooleanField(default=False)
    parking = models.BooleanField(default=False)
    bar = models.BooleanField(default=False)
    pool = models.BooleanField(default=False)
    availability = models.BooleanField(default=True)


class Car(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE, blank=True, null=True)
    destination = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True)
    PICK_UP_LOCATION = (
        ('1', 'Airport'),
        ('2', 'Hotel'),
        ('3', 'Agency'),
    )
    pick_up_location = models.CharField(max_length=256, choices=PICK_UP_LOCATION)
    pick_up_destination = models.CharField(max_length=256, default="")
    CAR_TYPES = (
        ('1', 'Coupe'),
        ('2', 'Sedan'),
        ('3', 'Van'),
    )
    car_types = models.CharField(max_length=256, choices=CAR_TYPES)
    mark = models.ForeignKey(to='adminPanel.CarMark', on_delete=models.SET_NULL, null=True)
    description = models.TextField()
    price = models.DecimalField(max_digits=50, decimal_places=2)
    number_of_sits = models.SmallIntegerField()
    picture1 = models.ImageField(upload_to='images/cars', default='images/brand-2.jpg')
    picture2 = models.ImageField(upload_to='images/cars', default='images/brand-2.jpg')
    permission = models.BooleanField(default=False)

    def __str__(self):
        return str(self.mark)


class AccommodationReview(models.Model):
    accommodation = models.ForeignKey(Accommodation, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    comment = models.TextField()
    stars = models.SmallIntegerField(default=5)
    permition = models.BooleanField(default=False)
    notification = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Accommodation Reviews"


class CarReview(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    comment = models.TextField()
    stars = models.SmallIntegerField(default=5)
    permition = models.BooleanField(default=False)
    notification = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name_plural = "Car Reviews"


class AccommodationOrder(models.Model):
    name = models.ForeignKey(Accommodation, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    accomodation_order_start_date = models.DateField()
    accomodation_order_end_date = models.DateField()


class AccomodationCheckout(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    accomodation_id = models.ForeignKey(Accommodation, on_delete=models.CASCADE)


class OrderCar(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    car_order_start_date = models.DateField()
    car_order_end_date = models.DateField()


class RoomPicture(models.Model):
    room = models.ForeignKey(HotelRoom, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to='images/accomodation')
